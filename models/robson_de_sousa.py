import os

lista = []


def ler_arquivo(filename):
    if os.path.exists(filename):
        with open(filename, "r") as file:
            linha = file.read()
            for x in linha:
                if x != ';' and x != '\n':
                    lista.append(x)
    file.close()


ler_arquivo("tabuleiro_dado.txt")


def converter_lista_matriz(li, n):
    return [li[i::n] for i in range(n)]


matriz = converter_lista_matriz(lista, 9)


def exibir_matriz():
    for x in matriz:
        print(x)


def resolver():
    encontrado = encontrar_posicao()
    if not encontrado:
        return True
    else:
        linha, coluna = encontrado

    for i in range(1, 10):
        if validar(i, (linha, coluna)):
            matriz[linha][coluna] = i

            if resolver():
                return True

            matriz[linha][coluna] = 0

    return False


def validar(num, pos):
    # Checar linha
    for i in range(len(matriz[0])):
        if matriz[pos[0]][i] == num and pos[1] != i:
            return False

    # Checar coluna
    for i in range(len(matriz)):
        if matriz[i][pos[1]] == num and pos[0] != i:
            return False

    # Check matriz
    pos_x = pos[1] // 3
    pos_y = pos[0] // 3

    for i in range(pos_y * 3, pos_y * 3 + 3):
        for j in range(pos_x * 3, pos_x * 3 + 3):
            if matriz[i][j] == num and (i, j) != pos:
                return False

    return True


def encontrar_posicao():
    for i in range(len(matriz)):
        for j in range(len(matriz[0])):
            if matriz[i][j] == '#':
                return i, j  # linha, coluna

    return None


def desenha_matriz():
    for i in range(len(matriz)):
        if i % 3 == 0 and i != 0:
            print("=========================")
        for j in range(len(matriz[0])):
            if j % 3 == 0 and j != 0:
                print(" || ", end="")

            if j == 8:
                print(matriz[i][j])
            else:
                print(str(matriz[i][j]) + " ", end="")


desenha_matriz()
resolver()
print("\n-------------------------\n")

desenha_matriz()


# exibir_matriz()


def escrever_arquivo(filename):
    with open(filename, "w") as file:
        for linha in matriz:
            linha = str(linha) + "\n"
            file.write(linha)


escrever_arquivo("resultado.txt")
